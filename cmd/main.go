package main

import "fmt"

type Address struct {
	street string
	city   string
}

type Student struct {
	matriculationNumber int
	name                string
	address             Address
	grades              map[string]float32
}

func (a *Address) String() string {
	return fmt.Sprintf("%s, %s", a.street, a.city)
}

func (s *Student) String() string {
	return fmt.Sprintf("%d %s, %s", s.matriculationNumber, s.name, s.address.String())
}

func (s *Student) AddGrade(lecture string, grade float32) {
	if s.grades == nil {
		s.grades = make(map[string]float32)
	}
	s.grades[lecture] = grade
}

func (s *Student) GetOverallAverageGrade() float32 {
	var sum float32
	for _, grade := range s.grades {
		sum += grade
	}

	return sum / float32(len(s.grades))
}

type SharedFlat struct {
	address  Address
	students []Student
}

func (s *SharedFlat) AddStudent(student Student) {
	s.students = append(s.students, student)
}

func (s *SharedFlat) GetNames() []string {
	var names []string
	for _, student := range s.students {
		names = append(names, student.String())
	}

	return names
}

func main() {
	address := Address{
		street: "Alteburgstraße 150",
		city:   "72762 Reutlingen",
	}

	student := Student{
		matriculationNumber: 7211,
		name:                "Max Müller",
		address:             address,
	}

	student.AddGrade("Enterprise Service Development", 1.0)
	student.AddGrade("Software Architecture", 1.2)

	fmt.Println(student.String())
	fmt.Println("Overall Average Grade:", student.GetOverallAverageGrade())

	sharedFlat := SharedFlat{
		address: address,
	}

	sharedFlat.AddStudent(student)
	fmt.Println(sharedFlat.GetNames())
}
